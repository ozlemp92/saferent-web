import React, { useState, useEffect } from 'react'
import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CContainer,
    CJumbotron,
    CRow, CImg,
    CEmbed,
    CEmbedItem, CInput, CInputGroup, CInputGroupText, CInputGroupPrepend
} from '@coreui/react';
import CIcon from '@coreui/icons-react'
// import FeatherIcon from 'feather-icons-react';
import { DocsLink } from 'src/reusable'
import '../../assets/css/style.css';
// import '../../assets/scss/style.scss';
import '../../assets/css/bootstrap.min.css';
import Loading from '../../shared/loading';
import NoDataFound from '../../shared/NoDataFound';
const Contracts = () => {
    const styleBackgroundWhite = {
        backgroundColor: "white"
    }
    const styleBackgroundGrey = {
        backgroundColor: "lightgrey"
    }

    const [contracts, setContracts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        setIsLoading(true);
        fetch("https://saferent.herokuapp.com/api/contract/landlord/123456789")
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    setContracts(result);
                },
                (error) => {
                    setContracts(null);
                    alert("hata");
                }
            );
        setIsLoading(false);
    }, []);


    return (
        contracts ===null?
            <NoDataFound text2={"Görünüşe göre SafeRent ile yapılan hiçbir sözleşmeniz bulunmuyor."}
            text3={"Yeni sözleşme için bizimle iletişime geçin"}
              />
             :(
            
        <Loading loadingText={"Yükleniyor"} loading={isLoading}>
        {
            
        <div className="rounded shadow p-3">
            <div className="d-flex align-items-center justify-content-between p-3">
                <div className="section-title">
                    <h4 className="title mb-2">Sözleşmelerim</h4>
                </div>
         
            </div>
            <div className="table-responsive bg-white rounded shadow">
                <table className="table table-center table-padding mb-0">
                    <thead>
                        <tr>
                            <th className="text-center py-3" style={{ minWidth: 160 }}>Kiracı</th>
                            <th className="text-center py-3" style={{ minWidth: 160 }}>Başlangıç Tarihi</th>
                            <th className="text-center py-3" style={{ minWidth: 160 }}>Sonlanma Tarihi</th>
                            <th className="text-center py-3" style={{ minWidth: 160 }}>Kira Tutarı</th>
                            <th className="text-center py-3" style={{ minWidth: 60 }}> Durumu</th>
                        </tr>
                    </thead>

                    <tbody style={{ padding: 5 }}>
                        {contracts.map(item => (
                            <tr>
                                <td className="text-center">
                                    {item.tenant.name + ' ' + item.tenant.surname}
                                </td>
                                <td className="text-center">
                                    {item.startDate}
                                </td>
                                <td className="text-center">
                                    {item.endDate}
                                </td>
                                <td className="text-center font-weight-bold">
                                    {item.rentAmount}
                                </td>
                                <td className="text-center font-weight-bold">
                                    {item.contractStatus === "STARTED" ?
                                        <li className="badge badge-success badge-pill">Aktif</li>
                                        :
                                        <li className="badge badge-danger badge-pill">Pasif</li>
                                    }
                                </td>
                                <td className="text-center">
                                    <div class="btn-group dropdown-primary">
                                     
                                        <div class="dropdown-menu">
                                            <a href="javascript:void(0)" class="dropdown-item"><i data-feather="eye-off" class="fea icon-sm"></i> Mark Unread</a>
                                            <a href="javascript:void(0)" class="dropdown-item"><i data-feather="corner-up-left" class="fea icon-sm"></i> Reply</a>
                                            <a href="javascript:void(0)" class="dropdown-item"><i data-feather="corner-up-right" class="fea icon-sm"></i> Forward</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="javascript:void(0)" class="dropdown-item text-danger"><i data-feather="trash-2" class="fea icon-sm"></i> Delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
               }
               </Loading>
             )
    )
}

export default Contracts