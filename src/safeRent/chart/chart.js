import React, { Component } from 'react';
import LineChart from 'react-linechart';
import * as ReactDOM from 'react-dom';
import {
  Chart,
  ChartTitle,
  ChartSeries,
  ChartSeriesItem,
  ChartCategoryAxis,
  ChartCategoryAxisItem
} from '@progress/kendo-react-charts';
import {
  CCard,
  CCardBody,
  CCardGroup,
  CCardHeader
} from '@coreui/react'
import {
  CChartBar,
  CChpartLine,
  CChartDoughnut,
  CChartRadar,
  CChartPie,
  CChartPolarArea
} from '@coreui/react-chartjs';
import Loading from '../../shared/loading';
import { getReport, occupationTypes } from '../../repository/repository';
import NoDataFound from '../../shared/NoDataFound';
import { green } from 'color-name';

class DashboardChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      occupationTypes: [],
      style: 'normal',
      chartLabel: [],
      chartData: [],
      txData: [],
      loading: true,
      data: { netGain: 0, income: 0, expense: 0 },
      isDataNull: false

    }
    this.setData = this.setData.bind(this);

  }
  setData = (res) => {
    let label = [];
    let cdata = [];
    var data = Object.keys(res.monthlyTransactions);
    var months = ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran",
      "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"];
    data.sort(function (a, b) {
      var month = months.indexOf(a) - months.indexOf(b);
      return month;
    });
    data.forEach((item, index) => {
      label.push(item);
      let before = 0;
      if (index > 0) {
        before = cdata[index - 1];
      }
      cdata.push(before + res.monthlyTransactions[item]);
    });
    this.setState({ chartLabel: label, chartData: cdata, data: res, txData: res.transactions });
  }
  componentDidMount() {
    var identity = localStorage.getItem("identityNumber");
    getReport(identity).then(res => {
      if (res.length !== 0) this.setData(res);
      else if (res.length !== 0) this.setState({ isDataNull: true });
    }).catch(err => {
      this.setState({ loading: false })
    }).finally(
      this.setState({ loading: false })
    )
  }

  render() {
    const styleBackgroundWhite = {
      backgroundColor: "white",
    }
    const styleBackgroundGrey = {
      backgroundColor: "lightgrey"
    }
    const styleBackgroundGreyRow = {
      backgroundColor: "lightgrey"
    }
const colorRed={
  color:"red"
}
const colorGreen={
  color:"green"
}
    return (

      this.state.isDataNull ?
        <div style={{ justifyContent: "center", width: "100%" }}>
          <NoDataFound text2={"Görünüşe göre hiçbir müşteri datası bulunmamaktadır"}
            text3={"Lütfen bizimle iletişime geçin!"}
          />
        </div>
        : (
          <div style={{ width: "70%", margin:'auto' }} >
            <><Loading style={{ width: "100%" }} loadingText={"Yükleniyor"} loading={this.state.loading}>
              {
                <>
                  <div style={{ display: "flex", flexDirection: "row", width: "100%" }}>
                    <div style={{ display: "flex", flexDirection: "column", marginTop: "-20px", width: "100%" }}>
                      <CCard style={{}}>
                        <CCardBody>
                          <CChartBar
                            datasets={[
                              {
                                label: 'Müşteri Raporu',
                                backgroundColor: '#6629e7',
                                data: this.state.chartData,
                                backgroundGradientFrom: "#6629e7",
                                backgroundGradientTo: "#6629e7"

                              }
                            ]}
                            labels={this.state.chartLabel}
                            options={{
                              tooltips: {
                                enabled: true
                              }
                            }}
                          />
                        </CCardBody>
                      </CCard>
                    </div>
                  </div>
                  <div className="rounded shadow p-3">
            <div className="d-flex align-items-center justify-content-between p-3">
                <div className="section-title">
                    <h4 className="title mb-2">İşlemlerim</h4>
                </div>
         
            </div>
            <div style={{display:"table"}} className="table-responsive bg-white rounded shadow">
                  <div className="row" style={(styleBackgroundGreyRow)}>
                    <div class="d-flex border-bottom p-3" style={{ width: "100%" }} >
                      <div class="media ml-2" style={{ flexGrow: "4" }}>
                        <div class="content ml-3" style={{
                          height: "55px",
                          borderRight: "1px solid black", width: "20%"
                        }}>
                          <h6 class="text-dark" style={{ marginLeft: "10px" }}>İŞLEM TARİHİ</h6>

                        </div>
                        <div class="content ml-3" style={{
                          height: "55px", borderRight: "1px solid black",
                          width: "30%"
                        }}>
                          <h6 class="text-dark">İŞLEM DETAYI</h6>
                        </div>
                        <div class="content ml-3">
                          <p class="text-dark">TUTAR</p>
                        </div>
                      </div>
                    </div>

                  </div>
                  {this.state.txData.map((x, index) => {
                    return (
                      <div className="row" style={x.id % 2 === 0 ? styleBackgroundGrey : styleBackgroundWhite}>
                        <div class="d-flex border-bottom p-3" style={{ width: "100%" }} >
                          <div class="media ml-2" style={{ flexGrow: "4" }}>
                            <div class="content ml-3" style={{
                              height: "55px",
                              borderRight: "1px solid black", width: "20%"
                            }}>
                              <h6 class="text-dark" style={{ marginLeft: "10px" }}>{x.transactionDate && x.transactionDate.split("-")[0]}</h6>
                              <h6 class="text-dark" style={{ marginTop: "-10px" }}>{x.transactionDate && x.transactionDate.split("-")[1]}</h6>
                              <h6 class="text-dark" style={{ marginTop: "-10px" }}>{x.transactionDate && x.transactionDate.split("-")[2]}</h6>

                            </div>
                            <div class="content ml-3" style={{ height: "55px", borderRight: "1px solid black", width: "30%" }}>
                              <h6 class="text-dark">{x.transactionDesc}</h6>
                            </div>
                            <div class="content ml-3">
                              <p class="" style={x.transactionType === "INCOME"? colorRed:colorGreen}>{x.amount}</p>
                            </div>
                          </div>
                        </div>

                      </div>
                    )
                  })}
                  
                   </div>
                  </div>
                </>
                
              }
              
            </Loading>

            </>
          </div>
        ))
  }
}




export default (DashboardChart);