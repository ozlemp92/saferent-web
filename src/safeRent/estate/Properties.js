import React from 'react'
import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CContainer,
    CJumbotron,
    CRow, CImg,
    CEmbed,
    CEmbedItem, CInput, CInputGroup, CInputGroupText, CInputGroupPrepend
} from '@coreui/react';
import CIcon from '@coreui/icons-react'
// import FeatherIcon from 'feather-icons-react';
import { DocsLink } from 'src/reusable'
import '../../assets/css/style.css';
// import icon from '../../src/assets/icons/ceo.jpg'
import '../../assets/css/bootstrap.min.css';
import '../../assets/css/materialdesignicons.min.css';
import '../../assets/css/magnific-popup.css';
import estates from '../../jsonFiles/myEstates.json';
import Loading from '../../shared/loading';
import icon from '../../assets/icons/ceo.jpg';
import { getProperties } from '../../repository/repository';
import NoDataFound from '../../shared/NoDataFound';
class Properties extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            occupationTypes: [],
            style: 'normal',
            loading: true,
           isDataNull:false,
           estates:[]
        }
     
    }
    componentDidMount() {
            var identity= localStorage.getItem("identityNumber");
            getProperties(identity).then(res => {
                 if (res.length !== 0)  this.setState({ estates: res });
                 else if (res.length === 0) this.setState({ isDataNull: true });
               }).catch(err => {
                 this.setState({ loading: false })
               }).finally(
                 this.setState({ loading: false })
               )
             }
           
      
      
    render() {
        return (
           //servis bağlanırken kontrolü yapılmalıdır. 
      this.state.isDataNull ?
      <div style={{justifyContent:"center", width:"100%"}}>
        <NoDataFound text2={"Görünüşe göre hiçbir mevcut mülkünüz bulunmuyor."}
        text3={"Lütfen bizimle iletişime geçin!"}
        />
      </div>
      : (
            <Loading loadingText={"Yükleniyor"} loading={this.state.loading}>
                {
                    <div className="row">
                        <div className="col-12 mt-4 pt-2">
                            <div className="section-title">
                                <h5 className="mb-0">Mülklerim :</h5>
                            </div>
                        </div>
                        {this.state.estates.map((item) => {
                            return (
                                <div className=" col-md-6 col-12 mt-4 pt-2">

                                    <div className="card job-box rounded shadow border-0 overflow-hidden">
                                        <div className="border-bottom" style={{ marginTop: "5%" }}>
                                            <div className="position-relative">
                                                <img src={item.flatImage} className="" alt=""></img>
                                                <div className="job-overlay bg-white"></div>
                                            </div>
                                            <h5 className="mb-0 position"><a href="page-job-detail.html" className="text-dark">{item.province + ' / ' + item.city}</a>
                                            </h5>
                                            <ul className="list-unstyled head mb-0">
                                                <li className={item.tenantName !== undefined? "badge badge-success badge-pill" : "badge badge-danger badge-pill"}>
                                                {item.tenantName !== undefined?"Kiralandı":"Kiracı Yok"}</li>
                                            </ul>
                                        </div>

                                        <div className="card-body content position-relative" style={{ borderTop: "1px solid #dee2e6!important" }} >
                                            <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between", flex: 2 }}>
                                                <div style={{ display: "flex", flexDirection: "column", flex: 1, justifyContent: "space-between", position: "absolute" }} className="rounded-circle shadow bg-light text-center">
                                                    <CImg style={{ borderRadius: "30px", width:"60px",height:"60px" }}
                                                        src={item.flatImage}
                                                        className=""
                                                        alt=""
                                                    />
                                                </div>
                                                <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between", flex: 1 }}>
                                                    <ul className="job-facts list-unstyled" style={{ marginLeft: "15%" }} >
                                                
                                                        <li className="list-inline-item text-muted"><i data-feather="check" className="fea icon-sm text-success mr-1"></i> {'Kiracı'}:</li>
                                                        <li className="list-inline-item text-muted"><i data-feather="check" className="fea icon-sm text-success mr-1"></i>{':  ' + (item.tenantName ? item.tenantName : ' - ') + ' ' +(item.tenantSurname ? item.tenantSurname : ' - ')}</li>
                                                        <br/>
                                                        <li className="list-inline-item text-muted"><i data-feather="check" className="fea icon-sm text-success mr-1"></i> {'İl/İlçe'}</li>
                                                        <li className="list-inline-item text-muted"><i data-feather="check" className="fea icon-sm text-success mr-1"></i>   {':  ' + item.city + ' / ' + item.province}</li>
                                                        <br/>
                                                        <li className="list-inline-item text-muted"><i data-feather="check" className="fea icon-sm text-success mr-1"></i> {'Kat'}</li>
                                                        <li className="list-inline-item text-muted"><i data-feather="check" className="fea icon-sm text-success mr-1"></i>    {':  ' + (item.tenantName ? '2' : '-')}</li>
                                                        <br/>
                                                        <li className="list-inline-item text-muted"><i data-feather="check" className="fea icon-sm text-success mr-1"></i> {'Adres'}</li>
                                                        <li className="list-inline-item text-muted"><i data-feather="check" className="fea icon-md text-success mr-1"></i>  {':  ' + item.address}</li>
                                   
                                            
                                                    </ul>
                                                </div>

                                            </div>
                              </div>
                                    </div>
                                </div>
                            )
                        })}


                    </div>
                }
            </Loading>
        ))
    }
}
export default Properties
