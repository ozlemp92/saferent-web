import React from 'react'
import {
  CCarousel,
  CCarouselCaption,
  CCarouselControl,
  CCarouselIndicators,
  CCarouselInner,
  CCarouselItem,
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CContainer,
  CJumbotron,
  CRow, CImg,
  CEmbed,
  CEmbedItem, CInput, CInputGroup, CInputGroupText, CInputGroupPrepend
} from '@coreui/react';
import CIcon from '@coreui/icons-react'
// import FeatherIcon from 'feather-icons-react';
import { DocsLink } from 'src/reusable'
import '../../assets/css/style.css';
import Loading from '../../shared/loading';
import '../../assets/css/bootstrap.min.css';
import '../../assets/css/materialdesignicons.min.css';
import '../../assets/css/magnific-popup.css';
import transactionsList from '../../jsonFiles/transactions.json';
import '../../assets/css/colors/default.css';
import NoDataFound from '../../shared/NoDataFound';
import Carousel from 'react-elastic-carousel';
import { getReport, getTransactions } from '../../repository/repository';
// import Carousel from 'nuka-carousel';
class transactions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isDataNull: false,
      txData: [],
      data: []
    }

  }
  componentDidMount() {
    var identity = localStorage.getItem("identityNumber");
    getTransactions(identity).then(res => {
      debugger;
      if (res.length !== 0) this.setState({ txData: res[0].transactions, data: res })
      else if (res.length !== 0) this.setState({ isDataNull: true });
    }).catch(err => {
      this.setState({ loading: false })
    }).finally(
      this.setState({ loading: false })
    )
  }

  render() {
    const styleBackgroundWhite = {
      backgroundColor: "white"
    }
    const styleBackgroundGreyRow = {
      backgroundColor: "lightgrey"
    }

    const styleBackgroundGrey = {
      backgroundColor: "lightgrey"
    }
    const colorRed = {
      color: "red"
    }
    const colorGreen = {
      color: "green"
    }
    return (
      this.state.isDataNull ?
        <div style={{ justifyContent: "center", width: "100%" }}>
          <NoDataFound text2={"Görünüşe göre hiçbir işlem datası bulunmamaktadır"}
            text3={"Lütfen bizimle iletişime geçin!"}
          />
        </div>
        : (
          <Loading loadingText={"Yükleniyor"} loading={this.state.loading}>
            {
              <>
                {/* <CRow>
                  <CCol xs="12" xl="6">
                    <CCard>
                      <CCardHeader>
                        Carousel with controls
            <DocsLink name="CCarousel" />
                      </CCardHeader>
                      <CCardBody>
                        <CCarousel>
                          <CCarouselInner>
                            <CCarouselItem>
                               <img className="d-block w-100" src={this.state.data[0]} alt="slide 1" />
                            </CCarouselItem>
                          
                          </CCarouselInner>
                          <CCarouselControl direction="prev" />
                          <CCarouselControl direction="next" />
                        </CCarousel>
                      </CCardBody>
                    </CCard>
                  </CCol>
                  </CRow> */}
                <Carousel> 
                  {this.state.data.map((item, index) => {
                    return (
                      <div className="row" style={{width:"700px"}}>
                        <div className="" style={{ width: "200%" }}>
                          <div style={{ marginTop: "-20px" }} className="card features explore-feature p-4 px-md-3 border-0 rounded-md shadow text-center">
                            <div style={{ marginTop: "-30px" }} className="card-body p-0 content">
                              <h5 className="mt-4" style={{ textAlign: "left", borderBottom: "2px solid lightgray" }} >{item.property.city + "/" + item.property.province}</h5>

                            </div>
                            <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between" }} >
                              <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
                                <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
                                  <label>Kazanç</label>
                                </div>
                                <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
                                  <label>{item.expense}</label>
                                </div>
                              </div>

                              <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
                                <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
                                  <label>Gelir</label>
                                </div>
                                <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
                                  <label>{item.income}</label>
                                </div>
                              </div>

                              <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
                                <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
                                  <label>Gider</label>

                                </div>
                                <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
                                  <label>{item.netGain}</label>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    )
                  })}  
                   </Carousel> 
                <div className="row"><h6 className="text-dark" style={{ marginLeft: "10px" }}>İŞLEMLERİM:</h6></div>
                <div className="row" style={(styleBackgroundGreyRow)}>
                  <div className="d-flex border-bottom p-3" style={{ width: "100%" }} >
                    <div className="media ml-2" style={{ flexGrow: "4" }}>
                      <div className="content ml-3" style={{
                        height: "55px",
                        borderRight: "1px solid black", width: "20%"
                      }}>
                        <h6 className="text-dark" style={{ marginLeft: "10px" }}>İŞLEM TARİHİ</h6>

                      </div>
                      <div className="content ml-3" style={{
                        height: "55px", borderRight: "1px solid black",
                        width: "30%"
                      }}>
                        <h6 className="text-dark">İŞLEM DETAYI</h6>
                      </div>
                      <div className="content ml-3">
                        <p className="text-dark">TUTAR</p>
                      </div>
                    </div>
                  </div>

                </div>
                {this.state.txData.map((x, index) => {
                  return (
                    <div className="row" style={index % 2 !== 0 ? styleBackgroundGrey : styleBackgroundWhite}>
                      <div className="d-flex border-bottom p-3" style={{ width: "100%" }} >
                        <div className="media ml-2" style={{ flexGrow: "4" }}>
                          <div className="content ml-3" style={{
                            height: "55px",
                            borderRight: "1px solid black", width: "20%"
                          }}>
                            <h6 className="text-dark" style={{ marginLeft: "10px", fontWeight: "bold" }}>{x.transactionDate && x.transactionDate.split("-")[0]}</h6>
                            <h6 className="text-dark" style={{ marginTop: "-10px" }}>{x.transactionDate && x.transactionDate.split("-")[1]}</h6>
                            <h6 className="text-dark" style={{ marginTop: "-10px" }}>{x.transactionDate && x.transactionDate.split("-")[2]}</h6>

                          </div>
                          <div className="content ml-3" style={{ height: "55px", borderRight: "1px solid black", width: "30%" }}>
                            <h6 className="text-dark">{x.transactionDesc}</h6>
                          </div>
                          <div className="content ml-3">
                            <p className="" style={x.transactionType === "INCOME" ? colorRed : colorGreen}>{x.amount}</p>
                          </div>
                        </div>
                      </div>

                    </div>
                  )
                })}

                <div className="col-12 mt-4 pt-2">
                  <ul className="pagination justify-content-center mb-0">
                    <li className="page-item"><a className="page-link" aria-label="Previous"><i className="mdi mdi-arrow-left"></i> Prev</a></li>
                    <li className="page-item active"><a className="page-link">1</a></li>
                    <li className="page-item"><a className="page-link">2</a></li>
                    <li className="page-item"><a className="page-link">3</a></li>
                    <li className="page-item"><a className="page-link" aria-label="Next">Next <i className="mdi mdi-arrow-right"></i></a></li>
                  </ul>
                </div>
              </>
            }
          </Loading>
        )
    )
  }
}
export default transactions