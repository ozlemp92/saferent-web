import React from 'react'
import { Link } from 'react-router-dom';
import '../../../assets/css/style.css';
import '../../../assets/css/bootstrap.min.css';
import '../../../assets/css/materialdesignicons.min.css';
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { withRouter } from 'react-router-dom';
// import Loading from '../../../shared/Loading';
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.logout();
    
        this.state = {
          identityNumber: '',
          phone: '',
          submitted: false,
          isLogin: false,
          loading:false
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    
      }
      componentDidMount()
      {
        localStorage.setItem('jwtToken', "user")
      }
     
      login = (identityNumber, phone) => {
        setTimeout(
            () => this.setState({ loading: true }),
            10000
        );
        const userData = {
            identityNumber: identityNumber,
            phone: phone
        }
        debugger;
        localStorage.setItem('identityNumber', identityNumber)
        setTimeout(
            () => this.setState({ loading: true }),
            1000, this.props.history.push('/dashboard')
        );
      }
    
      logout() {
        localStorage.removeItem('identityNumber');
      }
      handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
      }
      handleSubmit(e) {
        setTimeout(
            () => this.setState({ loading: true }),
            1000
        );
        e.preventDefault();
        const { identityNumber, phone } = this.state;
        if (identityNumber) {  
            debugger;
         this.login(identityNumber, phone);
         this.setState({ loading:true });
         //
        }
      }
    
    render() {
        return (
            <div style={{width:"100%"}}>

           
            {/* <Loading loadingText={"Yükleniyor"} loading={this.state.loading}>
            { */}
    <div>

    <section className="c-app c-default-layout flex-row align-items-center">
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-lg-5 col-md-8">
                    <div className="card login-page bg-white shadow rounded border-0">
                        <div className="card-body">
                            <h4 className="card-title text-center">Giriş Yapın</h4>
                            <form className="login-form mt-4">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="form-group position-relative">
                                            <label> Kimlik Numarası <span className="text-danger">*</span>
                                            </label>
                                            {/* <i data-feather="user" className="fea icon-sm icons"></i> */}
                                            <CInputGroup className="mb-3">
                                                <CInputGroupPrepend>
                                                    <CInputGroupText style={{ backgroundColor: "white" }}>
                                                        <CIcon name="cil-user" />
                                                    </CInputGroupText>
                                                </CInputGroupPrepend>
                                                <CInput type="text" placeholder="Kimlik Numarası"
                                                value={this.state.identityNumber} onChange={(e) => { this.setState({ identityNumber: 
                                                    e.target.value }) }}
                                                autoComplete="identitynumber" />
                                            </CInputGroup>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group position-relative">
                                        <label>Telefon Numarası <span className="text-danger">*</span>
                                            </label>
                                            <CInputGroup className="mb-4">
                                                <CInputGroupPrepend>
                                                    <CInputGroupText style={{ backgroundColor: "white" }}>
                                                        <CIcon name="cil-lock-locked" />
                                                    </CInputGroupText>
                                                </CInputGroupPrepend>
                                                <CInput type="text"
                                                 value={this.state.phone} onChange={(e) => { this.setState({ phone: 
                                                    e.target.value }) }}
                                                 placeholder="Telefon Numarası" autoComplete="current-password" />
                                            </CInputGroup>
                                        </div>
                                    </div>
                                    <div className="col-lg-12 mb-0">
                                        <button onClick={this.handleSubmit}  className="btn btn-primary btn-block">Giriş</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
    {/* }
    </Loading> */}
    </div>
  )
  
}
}
  export default withRouter(Login)
