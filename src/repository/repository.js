import axios from 'axios'

export const saveCredit = async (data) => {
    let result = await axios.post("https://saferent.herokuapp.com" + '/api/credit', data);
    return result.data;
}
export const sendSms = async (data) => {
    let result = axios.post("https://saferent.herokuapp.com" + '/api/sms/send', data);
    return result.data.data;
}
export const getReport = (id) => {
    debugger;

    return axios.get('https://saferent.herokuapp.com' + '/api/dashboard/report/' + id).then(response => {
        return response.data;
    })
}
export const getProperties = (id) => {
    return axios.get('https://saferent.herokuapp.com' + '/api/property/landlord/' + id).then(response => {
        return response.data;
    })
}
export const getTransactions = (id) => {
    return axios.get('https://saferent.herokuapp.com' + '/api/property/landlord/transactions/' + id).then(response => {
        return response.data;
    })
}
export const occupationTypes = () => {
    return axios.get('https://saferent.herokuapp.com' + '/api/util/occupation').then(response => {
        return response.data;
    })
}
