import React, { useState, useEffect } from 'react'
import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CContainer,
    CJumbotron,
    CRow, CImg,
    CEmbed,
    CEmbedItem, CInput, CInputGroup, CInputGroupText, CInputGroupPrepend, CLabel
} from '@coreui/react';
import CIcon from '@coreui/icons-react'
// import FeatherIcon from 'feather-icons-react';
import { DocsLink } from 'src/reusable'
import '../assets/css/style.css';
// import '../../assets/scss/style.scss';
import '../assets/css/bootstrap.min.css';
import Loading from './loading';
import icon from '../assets/icons/no-data.svg'
class NoDataFound extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            occupationTypes: [],
            style: 'normal',
            chartLabel: [],
            chartData: [],
            txData: [],
            loading: false,
            data: { netGain: 0, income: 0, expense: 0 },
        }

    }
    render() {
        return (
    
            <Loading loadingText={"Yükleniyor"} loading={this.state.loading}>
                {<>
                    <div style={{ display: "flex", flexDirection: "row", justifyContent: "center",marginTop:"-100px" }}>
                      

                    </div>
                    <div style={{ display: "flex", flexDirection: "row", justifyContent: "center" }}>
                            <CImg name="logo" to="/dashboard"
                                src={icon}
                                alt=""
                                style={{ marginBottom: 40, marginTop: 100, width: 180, height: 180 }}></CImg>
                        </div>
                        <div style={{ display: "flex", flexDirection: "row", justifyContent: "center" }}>
                            <CLabel style={{ color: '#000', fontWeight: 'bold', fontSize: 18 }}>{'Hiç sonuç bulamadık.'}</CLabel>

                        </div>
                        <div style={{ display: "flex", flexDirection: "row", justifyContent: "center" }}>
                            <CLabel style={{ color: '#aaa', textAlign: 'center', marginTop: 30 }}>{this.props.text2}</CLabel>

                        </div>
                        <div style={{ display: "flex", flexDirection: "row", justifyContent: "center" }}>
                            <CLabel style={{ color: '#6629e7', fontSize: 16, marginTop: 30 }}>
                            {this.props.text3}
  </CLabel>
                        </div>
                </>
                }
            </Loading>
             
        )
    }
}

export default NoDataFound