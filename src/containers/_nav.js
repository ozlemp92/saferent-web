export default [
  
  {
    _tag: 'CSidebarNavItem',
    name: 'Anasayfa',
    to: '/dashboard',
    icon: 'cil-home'
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Mülklerim',
    to: '/estate',
    icon: 'cil-spreadsheet',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Sözleşmelerim',
    to: '/contracts',
    icon: 'cil-notes',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'İşlemlerim',
    to: '/transactions',
    icon: 'cil-share',
  }
]
