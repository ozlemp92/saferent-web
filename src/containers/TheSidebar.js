import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem, CImg, CLabel,CLink
} from '@coreui/react'

import CIcon from '@coreui/icons-react'
import icon from '../../src/assets/icons/ceo.jpg'
// sidebar nav config
import navigation from './_nav'

const TheSidebar = () => {
  const dispatch = useDispatch()
  const show = useSelector(state => state.sidebarShow)

  return (
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch({ type: 'set', sidebarShow: val })}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
      <br />

        <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }} >
          <div className="c-avatar" style={{marginLeft:"20px", marginTop:"10px" ,display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
          <CLink to="/dashboard">
        <CSidebarBrand>
        <CImg name="logo" to="/dashboard" 
              src={icon}
              className="c-avatar-img"
              alt=""
            />
        </CSidebarBrand>
      </CLink>
           
            <br />

          </div>
          <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
            <CLabel htmlFor="">Yaşar YAŞA</CLabel>
          </div>

        </div>

        <CIcon
          className="c-sidebar-brand-minimized"
          name="sygnet"
          height={35}
        />
      </CSidebarBrand>
      <CSidebarNav>

        <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none" />
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
