import React from 'react'
import {
  TheContent,
  TheSidebar,
  TheFooter,
  TheHeader,
  MySidebar,
} from './index'
import "../assets/css/bootstrap.min.css";

const TheLayout = () => {
  return (
    <div className="c-app c-default-layout">
      <TheSidebar/>
      {/* <MySidebar/> */}
      <div className="c-wrapper" style={{backgroundColor:'#fff'}}>
        <TheHeader />
        <section className="section" style={{padding:0}}>
          <div className="container">
            {/* <div className="c-body"> */}
            <TheContent />
          </div>
        </section>
        {/* <TheFooter /> */}
      </div>
    </div>
  )
}

export default TheLayout
